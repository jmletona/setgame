//
//  SetGame.swift
//  SetGame
//
//  Created by José Letona Rodríguez on 9/17/21.
//

import UIKit
import Foundation

class SetGame {
    // MARK: - Properties
    var deck = [Card]()
    private let numberOfCardsToStart = 12
    private let maxCardsToShow = 24
    private(set) var score = 0
    private(set) var taps = 0
    var cardsSelected = [Card]()
    var setCardsMatched = [Card]()
    var setCardsMismatched = [Card]()
    var showCards = [Card?](repeating: nil, count: 24)
    private var startSetTime: Date?
    private var maxTimeToMisMatch = 10
    
    
    // MARK: - Initialization
    init (){
    }
    
    // MARK: - Functions
    func newGame(){
        deck.removeAll()
        cardsSelected.removeAll()
        setCardsMatched.removeAll()
        setCardsMismatched.removeAll()
        showCards = [Card?](repeating: nil, count: 24)
        createDeck()
        dealCards(numberOfCardToDeal: numberOfCardsToStart)
        score = 0
        taps = 0


        startSetTime = Date()
    }
    
    private func createDeck(){
        for symbol in Symbol.allCases {
            for color in Color.allCases {
                for number in Number.allCases {
                    for fill in Fill.allCases{
                        deck.append(Card(symbol: symbol, color: color, number: number, fill: fill))
                    }
                }
            }
        }
        deck.shuffle()
    }
    
    func tapACard(cardNumber: Int){
        guard  setCardsMismatched.contains(showCards[cardNumber]!) || setCardsMatched.contains(showCards[cardNumber]!) else {
            if cardsSelected.count < 3 {
                
                if( setCardsMatched.count > 0){
                    replaceCards()
                }
                setCardsMatched.removeAll()
                setCardsMismatched.removeAll()
                if let indexSelected = cardsSelected.firstIndex(of: showCards[cardNumber]!){
                    cardsSelected.remove(at: indexSelected)
                    taps += 1
                } else {
                    selectCard(cardNumber: cardNumber)
                    taps += 1
                }
            }
            if  cardsSelected.count == 3 {
                selectCard(cardNumber: cardNumber)
                checkSet()
            }
            return
        }
    }
    
    func replaceCards(){
        for card in setCardsMatched {
            if (deck.count>0){
                showCards[showCards.firstIndex(of: card)!] = deck[0]
                deck.remove(at: 0)
            } else {
                showCards[showCards.firstIndex(of: card)!] = nil
            }
        }


    }
    
    func checkSet() {
        if cardsMatched() {
            setCardsMatched = cardsSelected
            score += 3
            extraScoreforMatched()
        }
        else {
            extraScoreforMisMatched()
            setCardsMismatched = cardsSelected
            score -= 5
        }
        cardsSelected.removeAll()
    }
    func extraScoreforMatched() {
        let intervalTime = Calendar.current.dateComponents([.second], from: startSetTime!, to: Date()).second ?? 0
        switch intervalTime {
        case 0...10:
            score += 5
        case 0...20:
            score += 3
        default: break
        }
        startSetTime = Date()
    }
    
    func extraScoreforMisMatched() {
        var intervalTime = Calendar.current.dateComponents([.second], from: startSetTime!, to: Date()).second ?? 0
        if (intervalTime == 0) { intervalTime = 1 }
        switch intervalTime {
        case 1...maxTimeToMisMatch:
            score -= abs(maxTimeToMisMatch/intervalTime)
        default: break
        }
        startSetTime = Date()
    }
    









    
    func cardsMatched() -> Bool {
        let colors = Set(cardsSelected.map {$0.color})
        let symbols = Set(cardsSelected.map {$0.symbol})
        let  fills = Set(cardsSelected.map {$0.fill})
        let numbers = Set(cardsSelected.map {$0.number})
        return !((colors.count == 2) || (symbols.count == 2) || (fills.count == 2) || (numbers.count == 2))
    }
    
    func selectCard(cardNumber: Int) {
        if !cardsSelected.contains(showCards[cardNumber]!){
            cardsSelected.append(showCards[cardNumber]!)
        }
    }
    
    func isdealButtonEnabled() -> Bool {
        if (showCards.contains(nil) || setCardsMatched.count > 0) && deck.count >= 3{
            return true
        }
        
        return false
    }
    
    func dealCards(numberOfCardToDeal: Int){
        var counter = 0
        if deck.count < numberOfCardToDeal { return }
        if !showCards.contains(nil) { return  }
        for index in deck.indices {
            if showCards[index] == nil {
                showCards[index] = deck[0]
                deck.remove(at: 0)
                counter += 1
                if (counter >= numberOfCardToDeal){
                    break
                }
            }
        }
    }





    
    func getScore() -> String {
        return String(score)
    }
    
    func getTaps() -> String {
        return String(taps)
    }
    
    


}
