//
//  ViewController.swift
//  SetGame
//
//  Created by José Letona Rodríguez on 9/17/21.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var dealButton: UIButton!
    @IBOutlet private weak var tapsLabel: UILabel!
    @IBOutlet private weak var scoreLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]!
    @IBAction func tapNewGame(_ sender: UIButton) {
        newGame()
    }
    @IBAction func tapDeal(_ sender: UIButton) {
        if game.isdealButtonEnabled() {
            
            if game.setCardsMatched.count == 0 {
                game.dealCards(numberOfCardToDeal: numberOfCardToDeal)
            } else {
                game.replaceCards()
                game.setCardsMatched.removeAll()
                game.setCardsMismatched.removeAll()
            }
            updateViewFromModel()
        } else {
            game.dealCards(numberOfCardToDeal: numberOfCardToDeal)
        }
        if game.isdealButtonEnabled() {
            sender.isEnabled = false
        }
        sender.isEnabled = game.isdealButtonEnabled()
    }
    @IBAction func tapCard(_ sender: UIButton) {
        if game.isdealButtonEnabled() {
            dealButton.isEnabled = false
        }
        if game.setCardsMatched.count > 0 && game.deck.count > 0 {
            dealButton.isEnabled = true
        }
        if let cardNumber = cardButtons.firstIndex(of: sender){
            game.tapACard(cardNumber: cardNumber)
        }
        updateViewFromModel()
        dealButton.isEnabled = game.isdealButtonEnabled()
    }
    
    // MARK: - Properties
    var deck = CardDeck()
    lazy var game = SetGame()
    private let numberOfCardToDeal = 3
    private let symbol: [Symbol : String] = [ .square : "■", .triangle : "▲", .circle : "●" ]
    private let color: [Color : UIColor] = [ .purple : UIColor.purple, .red : UIColor.red, .green : UIColor.green ]
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        newGame()
        //print(deck)
    }
    
    // MARK: - Functions
    private func newGame(){
        game.newGame()
        dealButton.isEnabled = true
        updateViewFromModel()
    }
    
    private func updateViewFromModel(){
        cardButtons.enumerated().forEach(){
            (index: Int, cardButton: UIButton) -> () in customizeCard(index: index, cardButton: cardButton)
        }
        scoreLabel.text = "Score: \(game.getScore())"
        tapsLabel.text = "Taps: \(game.getTaps())"
    }
    
    
    private func customizeCard(index: Int, cardButton: UIButton) {
        if let card = game.showCards[index] {
            cardButton.alpha = 1
            
            if  game.cardsSelected.contains(card) {
                cardButton.layer.borderWidth = 4
                cardButton.layer.borderColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            }else if game.setCardsMatched.contains(card) {
                cardButton.layer.borderWidth = 4
                cardButton.layer.borderColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            }else if game.setCardsMismatched.contains(card) {
                cardButton.layer.borderWidth = 4
                cardButton.layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            }else {
                cardButton.layer.borderWidth = 1
                cardButton.layer.borderColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
            }
            cardButton.layer.cornerRadius = 8.0
            cardButton.layer.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            let quote = symbol[card.symbol]
            var finalText = ""
            let number = card.number.rawValue + 1
            for _ in 1...number {
                finalText += quote ?? ""
            }
            var attributes: [NSAttributedString.Key: Any] = [:]
            attributes[NSAttributedString.Key.foregroundColor] = UIColor.red.withAlphaComponent(0.8)
            switch card.fill {
            case .outline:
                attributes[NSAttributedString.Key.strokeWidth] = 4
                fallthrough
            case .filled:
                attributes[NSAttributedString.Key.foregroundColor] = color[card.color]
            case .striped:
                attributes[NSAttributedString.Key.foregroundColor] = color[card.color]?.withAlphaComponent(0.15)
            }
            attributes[NSAttributedString.Key.font] = UIFont.systemFont(ofSize: 40)
            let attributedQuote = NSAttributedString(string: finalText, attributes: attributes)
            cardButton.setAttributedTitle(attributedQuote, for: .normal)
        } else {
            cardButton.alpha = 0
        }
    }
}


