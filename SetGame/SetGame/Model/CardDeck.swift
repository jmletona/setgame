//
//  CardDeck.swift
//  SetGame
//
//  Created by José Letona Rodríguez on 9/22/21.
//

import Foundation

struct CardDeck {
    private(set) var cards = [Card]()
    
    init() {
        for symbol in Symbol.allCases {
            for color in Color.allCases {
                for number in Number.allCases {
                    for fill in Fill.allCases{
                        cards.append(Card(symbol: symbol, color: color, number: number, fill: fill))
                    }
                }
            }
        }
        //cards.shuffle()
    }
    
    
    mutating func draw() -> Card? {
        if cards.count > 0 {
            return cards.remove(at: cards.count.arc4random)
        } else {
            return nil
        }
    }
}
extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0{
            return -Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
