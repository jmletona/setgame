//
//  Card.swift
//  SetGame
//
//  Created by José Letona Rodríguez on 9/17/21.
//

import Foundation
import UIKit

struct Card: CustomStringConvertible {
    
    var description: String {
        return "\(number.rawValue+1) \(symbol.rawValue) \((color)) \(fill)\n" 
    }
    
    var symbol: Symbol
    var color: Color
    var number: Number
    var fill: Fill
    
    init(symbol: Symbol, color: Color, number: Number, fill: Fill) {
        self.symbol = symbol
        self.color = color
        self.number = number
        self.fill = fill
    }
    
    
}

enum Symbol: String, CaseIterable {
    case triangle =  "▲"
    case square = "■"
    case circle = "●"
}

enum Color: CaseIterable {
    case red
    case green
    case purple
}

enum Number: Int, CaseIterable {
    case one
    case two
    case three
}

enum Fill: CaseIterable {
    case striped
    case filled
    case outline
}

extension Card: Equatable {
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.number == rhs.number &&
              lhs.color == rhs.color &&
              lhs.symbol == rhs.symbol &&
              lhs.fill == rhs.fill
    }
}
